# KUBERNETES PLAYGROUND PROJECT
Setup different Kubernetes infrastructure using Vagrant and Ansible

## Technology
* Vagrant
* Ansible

## Cluster Information
* Kubernetes version: v1.23.1
* OS version: Ubuntu 20.04
* Container runtime: Containerd v1.6.7(latest)
* Container network interface: Flannel v0.19.1(latest)

## Setup Prerequisites
* A ssh key in .ssh/
* Virtualbox [install](https://linuxhint.com/install-setup-virtualbox-ubuntu-22-04/)
    ```
    $ sudo apt install virtualbox
    ```
* Vagrant [install](https://itslinuxfoss.com/install-vagrant-ubuntu-22-04/)
    ```
    $ sudo apt install vagrant
    ```
* Ansible [install](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-22-04)
    ```
    $ sudo apt install ansible
    ```
* Kubectl [install](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
    ```
    $ sudo apt-get update
    $ sudo apt-get install -y ca-certificates
    $ sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
    $ echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
    $ sudo apt-get update
    $ sudo apt-get install -y kubectl
    ```
* Helm [install](https://helm.sh/docs/intro/install/)
    ```
    $ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    $ chmod 700 get_helm.sh
    $ ./get_helm.sh
    ```