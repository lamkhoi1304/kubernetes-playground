# Create a Multi Master Kubernetes Cluster with Vagrant, Ansible, Kubeadm

## Setup Infrastructure
![./images/mmc.png](./images/mmc.png?raw=true "./images/mmc.png")

## Cluster Information
* Kubernetes version: v1.23.1
* OS version: Ubuntu 20.04
* Container runtime: Containerd v1.6.7(latest)
* Container network interface: Flannel v0.19.1(latest)

## System Requirements
* 3 masters, each master has 2 CPUs and 2 GBs RAM
* 3 workers, each worker has 2 CPUs and 2 GBs RAM
* 1 loadbalancer has 1 CPU and 1 GB RAM

## Setup Prerequisites
* A ssh key in .ssh/
* Virtualbox [install](https://linuxhint.com/install-setup-virtualbox-ubuntu-22-04/)
    ```
    $ sudo apt install virtualbox
    ```
* Vagrant [install](https://itslinuxfoss.com/install-vagrant-ubuntu-22-04/)
    ```
    $ sudo apt install vagrant
    ```
* Ansible [install](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-22-04)
    ```
    $ sudo apt install ansible
    ```
* Kubectl [install](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
    ```
    $ sudo apt-get update
    $ sudo apt-get install -y ca-certificates
    $ sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
    $ echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
    $ sudo apt-get update
    $ sudo apt-get install -y kubectl
    ```

## Provision VMs
```
$ vagrant up
```

## Create Kubernetes Cluster
```
$ cd ansible
$ ansible-playbook -i inventory main.yml
```

Download kubeconfig to local
```
$ mkdir ~/.kube-local
$ scp ci@192.168.56.11:/home/ci/.kube/config ~/.kube-local/config
$ chmod 600 ~/.kube-local/config
$ export KUBECONFIG=~/.kube-local/config
```
Verify cluster
```
$ kubectl get nodes -o wide
```