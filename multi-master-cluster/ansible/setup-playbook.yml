---

- hosts: masters, workers
  name: Setup masters and workers
  remote_user: ci
  become: true
  tasks:
    # Configuration
    # Disable swap
    - name: Disable swap
      shell: swapoff -a

    - name: Remove swap entry from /etc/fstab
      lineinfile:
        dest: /etc/fstab
        regexp: swap
        state: absent

    # Load overlay and br_netfilter modules
    - name: Load overlay and br_netfilter modules
      shell: |
        cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
        overlay
        br_netfilter
        EOF

    - name: Load overlay module
      shell: modprobe overlay

    - name: Load br_netfilter modules
      shell: modprobe br_netfilter

    # Configure iptables
    - name: Configure iptables to see bridged traffic
      shell: |
        cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
        net.bridge.bridge-nf-call-iptables = 1
        net.ipv4.ip_forward = 1
        net.bridge.bridge-nf-call-ip6tables = 1
        EOF

    - name: Read value from all system directories
      shell: sysctl --system

    # Install container runtime
    - name: Install packages to allow apt to use a repository over HTTPS
      apt:
        name:
          - ca-certificates
          - gnupg
          - apt-transport-https
        state: present
        update_cache: yes

    - name: Add Docker's official GPG key
      apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        state: present

    - name: Get ubuntu release version
      shell: lsb_release -cs
      register: ubuntu_version

    - name: Get architecture
      shell: dpkg --print-architecture
      register: architecture

    - name: Add Docker repository
      apt_repository:
        repo: deb [arch={{ architecture.stdout }}] https://download.docker.com/linux/ubuntu {{ ubuntu_version.stdout }} stable
        state: present
        filename: docker

    - name: Update apt packages
      apt:
        update_cache: yes
        force_apt_get: yes

    - name: Install containerd.io
      apt:
        name:
          - containerd.io
        state: present
        update_cache: yes

    - name: Create containerd directory
      file:
        path: /etc/containerd
        state: directory

    - name: Generate the default configuration file
      shell: |
        containerd config default | sudo tee /etc/containerd/config.toml

    - name: Restart containerd
      systemd:
        name: containerd
        state: restarted

    # Install kubeadm, kubelet and kubectl
    - name: Add google cloud public signing key
      apt_key:
        url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
        state: present

    - name: Add kubernetes repository
      apt_repository:
        repo: deb http://apt.kubernetes.io/ kubernetes-xenial main
        state: present
        filename: kubernetes

    - name: Update apt packages
      apt:
        update_cache: yes
        force_apt_get: yes

    - name: Install kubeadm, kubelet and kubectl version 1.23.1-00
      apt:
        name:
          - kubeadm=1.23.1-00
          - kubelet=1.23.1-00
          - kubectl=1.23.1-00
        state: present
        update_cache: yes

    # pods/log the server could not find the requested resource
    # Solution: edit the kubelet config
    # https://github.com/kubernetes/kubernetes/issues/60835#issuecomment-395931644
    - name: Edit kubeadm.conf
      blockinfile:
        path: /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
        block: |
          Environment="KUBELET_EXTRA_ARGS=--node-ip={{ inventory_hostname }}"