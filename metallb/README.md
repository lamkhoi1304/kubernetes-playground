# MetalLB

## Source: 
* https://metallb.universe.tf/installation/#installation-with-helm

## Prerequisites
* Helm [install](https://helm.sh/docs/intro/install/)
    ```
    $ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    $ chmod 700 get_helm.sh
    $ ./get_helm.sh
    ```
* Kubeconfig
    ```
    $ export KUBECONFIG=~/.kube-local/config
    ```
## Deploy Metallb

```
$ helm repo add metallb https://metallb.github.io/metallb
```

```
$ kubectl create ns metallb-system
```

```
$ helm install -n metallb-system metallb metallb/metallb
```

```
$ kubectl create -f values.yml
```