# Ingress Nginx

## Source: 
* https://github.com/kubernetes/ingress-nginx

## Prerequisites
* Helm [install](https://helm.sh/docs/intro/install/)
    ```
    $ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    $ chmod 700 get_helm.sh
    $ ./get_helm.sh
    ```
* Kubeconfig
    ```
    $ export KUBECONFIG=~/.kube-local/config
    ```
## Deploy Ingress Nginx
```
$ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
```

```
$ kubectl create ns ingress-nginx
```

```
$ helm install -n ingress-nginx ingress-nginx ingress-nginx/ingress-nginx --values values.yml
```