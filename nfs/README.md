# NFS Subdir External Provisioner

## Source: 
* https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner/blob/master/charts/nfs-subdir-external-provisioner/README.md

## Prerequisites
* Helm [install](https://helm.sh/docs/intro/install/)
    ```
    $ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    $ chmod 700 get_helm.sh
    $ ./get_helm.sh
    ```
* Kubeconfig
    ```
    $ export KUBECONFIG=~/.kube-local/config
    ```
## Deploy NFS Subdir External Provisioner
```
$ helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
```

```
$ kubectl create ns nfs-system
```

```
$ helm install -n nfs-system nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --set nfs.server=x.x.x.x --set nfs.path=/mnt/nfs_share
```