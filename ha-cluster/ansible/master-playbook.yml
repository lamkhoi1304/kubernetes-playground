---

- hosts: masters
  name: Init cluster
  remote_user: ci
  become: true
  tasks:
    # Reset kubernetes cluster
    - name: Reset existing cluster
      shell: kubeadm reset -f

    - name: Remove .kube in user home directory
      shell: rm -rf .kube

    - name: Remove /etc/kubernetes/manifests directory
      shell: rm -rf /etc/kubernetes/manifests

    - name: Remove /var/lib/etcd directory
      shell: rm -rf /var/lib/etcd

    # Init kubernetes cluster
    - name: Init kubernetes cluster
      shell: |
        kubeadm init --control-plane-endpoint=192.168.56.30:6443 --upload-certs --apiserver-advertise-address=192.168.56.11 --pod-network-cidr=10.244.0.0/16
      run_once: true
      delegate_to: "192.168.56.11"

    # Copy join command
    - name: Print join command
      shell: kubeadm token create --print-join-command
      register: kubernetes_join_command
      run_once: true
      delegate_to: "192.168.56.11"

    - name: Copy join command to local
      become: false
      local_action: copy content="{{ kubernetes_join_command.stdout_lines[0] }}" dest="/tmp/kubernetes_join_command" mode=0777
      run_once: true
      delegate_to: "192.168.56.11"

    - name: Generate certificate key
      shell: |
        kubeadm init phase upload-certs --upload-certs
      register: kubernetes_certificate_key
      run_once: true
      delegate_to: "192.168.56.11"

    - name: Execute master join command
      become: yes
      shell: "{{ kubernetes_join_command.stdout_lines[0] }} --control-plane --certificate-key {{ kubernetes_certificate_key.stdout_lines[2] }} --apiserver-advertise-address={{ inventory_hostname }}"
      when: inventory_hostname != "192.168.56.11"

    # Cannot access cluster IP
    # Solution: Add the iface name in kube-flannel.yml line 159
    # https://medium.com/@anilkreddyr/kubernetes-with-flannel-understanding-the-networking-part-1-7e1fe51820e4
    - name: Copy kube-flannel.yml
      copy:
        src: ./CNI/kube-flannel.yml
        dest: /home/ci/kube-flannel.yml
        owner: ci
        group: ci
        mode: 0644
      run_once: true
      delegate_to: "192.168.56.11"

    # Deploy flannel network
    - name: Deploy flannel network
      shell: |
        kubectl --kubeconfig=/etc/kubernetes/admin.conf apply -f ./kube-flannel.yml
      run_once: true
      delegate_to: "192.168.56.11"

    - name: Create directory for kube config
      file:
        path: /home/ci/.kube
        state: directory

    - name: Copy /etc/kubernetes/admin.conf to user home directory
      shell: |
        cp -i /etc/kubernetes/admin.conf /home/ci/.kube/config

    - name: Change permission config file
      file:
        path: /home/ci/.kube/config
        owner: ci
        group: ci
        mode: 0644

    - name: Restart kubelet service
      service:
        name: kubelet
        state: restarted
        daemon-reload: yes