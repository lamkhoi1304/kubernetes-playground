# Metrics Server

## Source: 
* https://github.com/kubernetes-sigs/metrics-server

## Prerequisites
* Helm [install](https://helm.sh/docs/intro/install/)
    ```
    $ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    $ chmod 700 get_helm.sh
    $ ./get_helm.sh
    ```
* Kubeconfig
    ```
    $ export KUBECONFIG=~/.kube-local/config
    ```
## Deploy Metrics Server
```
$ helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
```

```
$ kubectl create ns metrics-system
```

```
$ helm install -n metrics-system metrics-server metrics-server/metrics-server --values ./metrics-server/values.yml
```